<?php
$title = "Layanan - Studi dan Penelitian";
include "header.php";
include "topbar.php"; ?>

<main id="main">
    <section id="about">
        <div class="container">

            <div class="row about-extra">
                <div class="col-lg-12 wow fadeInUp pt-5 pt-lg-0">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index#intro">Home</a></li>
                            <li class="breadcrumb-item"><a href="index#services">Layanan</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?= $title; ?></li>
                        </ol>
                    </nav>
                    <h4>Studi dan Penelitian</h4>
                    <p>
                        (Studi Kelayakan, Studi Pasar, Studi Teknologi, Studi Lingkungan, Studi Kebijakan)
                    </p>

                    <br>
                    <h4>Pengalaman Proyek</h4>

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Partner Kerja</th>
                                <th scope="col">Jenis Kegiatan</th>
                                <th scope="col">Waktu</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td>PT. Toyota Motor Manufacturing Indonesia</td>
                                <td>Reserch for approriate frequency band for SMART/RKE and TPMS System </td>
                                <td>1 September 2016 s/d 31 Oktober 2016</td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>PLN Puslitbang</td>
                                <td>Cetak Biru Transpormasi PLN Puslitbang</td>
                                <td>25 Juli 2018 s/d 7 Oktober 2018</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </section><!-- #about -->
</main>

<?php include "footer.php"; ?>