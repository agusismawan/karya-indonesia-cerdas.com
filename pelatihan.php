<?php
$title = "Layanan - Pelatihan";
include "header.php";
include "topbar.php"; ?>

<main id="main">
    <section id="about">
        <div class="container">

            <div class="row about-extra">
                <div class="col-lg-12 wow fadeInUp pt-5 pt-lg-0">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index#intro">Home</a></li>
                            <li class="breadcrumb-item"><a href="index#services">Layanan</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?= $title; ?></li>
                        </ol>
                    </nav>
                    <h4>Pelatihan</h4>
                    <p>
                        (Teknologi dan Kebijakan)
                    </p>

                    <br>
                    <h4>Pengalaman Proyek</h4>

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col"></th>
                                <th scope="col">Partner Kerja</th>
                                <th scope="col">Jenis Kegiatan</th>
                                <th scope="col">Waktu</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row" rowspan="3">1</th>
                                <td><img src="img/partners/jsd.jpg" alt="" class="rounded" width="80px"></td>
                                <td>JS Denki Pte Ltd Singapore 658077</td>
                                <td rowspan="3">Training dan Workshop Tahap I berjudul: "Electromagnetic Compatibility (EMC) Introduction for Quality and Safety"</td>
                                <td rowspan="3">22 November 2017 s/d 24 November 2017</td>
                            </tr>
                            <tr>
                                <td><img src="img/partners/itb.png" alt="" class="rounded" width="40px"></td>
                                <td>Institut Teknologi Bandung</td>
                            </tr>
                            <tr>
                                <td><img src="img/partners/b4t.jpg" alt="" class="rounded" width="40px"></td>
                                <td>Balai Besar Bahan dan Barang Teknik (B4P) Bandung</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </section><!-- #about -->
</main>

<?php include "footer.php"; ?>