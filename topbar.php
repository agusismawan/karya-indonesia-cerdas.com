<!--==========================
  Header
  ============================-->
<header id="header" class="fixed-top">
    <div class="container">

        <div class="logo float-left">
            <a href="#intro" class="scrollto"><img src="img/logo800px.png" alt="" class="img-fluid"></a>
        </div>



        <nav class="main-nav float-right d-none d-lg-block">
            <ul>
                <li class="active"><a href="index#intro">Home</a></li>
                <li><a href="index#about">Tentang Kami</a></li>
                <li><a href="index#services">Layanan</a></li>
                <li><a href="index#portfolio">Portofolio</a></li>
                <!-- <li><a href="#team">Tim</a></li> -->
                <li><a href="index#contact">Hubungi Kami</a></li>
            </ul>
        </nav><!-- .main-nav -->

    </div>
</header><!-- #header -->