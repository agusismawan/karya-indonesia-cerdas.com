<?php
$title = "Layanan - Pengujian Evaluasi";
include "header.php";
include "topbar.php"; ?>

<main id="main">
    <section id="about">
        <div class="container">

            <div class="row about-extra">
                <div class="col-lg-12 wow fadeInUp pt-5 pt-lg-0">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index#intro">Home</a></li>
                            <li class="breadcrumb-item"><a href="index#services">Layanan</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?= $title; ?></li>
                        </ol>
                    </nav>
                    <h4>Pengujian dan Evaluasi</h4>
                    <p>
                        (Produk Teknologi, Sistem dan Kebijakan)
                    </p>

                    <br>
                    <h4>Pengalaman Proyek</h4>

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col"></th>
                                <th scope="col">Partner Kerja</th>
                                <th scope="col">Jenis Kegiatan</th>
                                <th scope="col">Waktu</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">1</th>
                                <td><img src="img/partners/jsd.jpg" alt="" class="rounded" width="130px"></td>
                                <td>JS Denki Pte Ltd Singapore 658077</td>
                                <td>Memasarkan dan mempromosikan EMC & RF Wireless Testing Measurement System dengan aktivitas terkait untuk area Indonesia</td>
                                <td>1 Maret 2016 s/d 1 Maret 2018</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </section><!-- #about -->
</main>

<?php include "footer.php"; ?>