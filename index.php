<?php
$title = "PT. Karya Indonesia Cerdas";
include "header.php";
include "topbar.php";
?>
<!--==========================
    Intro Section
  ============================-->
<section id="intro" class="clearfix">
  <div class="container">

    <!-- <div class="intro-img">
        <img src="img/intro-img.svg" alt="" class="img-fluid">
      </div> -->

    <div class="intro-info">
      <h2>Trusted company<br>for making <br>Indonesia <span>Smart</span><br></h2>
      <div>
        <a href="#about" class="btn-get-started scrollto">Memulai</a>
        <a href="#services" class="btn-services scrollto">Layanan Kami</a>
      </div>
    </div>

  </div>
</section><!-- #intro -->

<main id="main">

  <!--==========================
      About Us Section
    ============================-->
  <section id="about">
    <div class="container">

      <header class="section-header">
        <h3>Tentang Kami</h3>
        <p>PT. KARYA INDONESIA CERDAS adalah perusahaan yang menyediakan layanan Studi, Riset, Inovasi, Testing/Pengujian, Optimasi, Coaching/Training, Assesment, Supply dan Evaluasi. Layanan Perusahaan kami merupakan solusi untuk Energi, Telekomunikasi, Minyak dan Gas, Kesehatan, Transportasi, Manufaktur dan Pemerintahan.
        </p>
      </header>

      <div class="row about-container">

        <div class="col-lg-6 content order-lg-1 order-2">
          <div class="icon-box wow fadeInUp">
            <div class="icon"><i class="fa fa-location-arrow"></i></div>
            <h4 class="title"><a href="">Visi</a></h4>
            <p class="description">“Trusted company for making Indonesia Smart”</p>
          </div>

          <div class="icon-box wow fadeInUp" data-wow-delay="0.2s">
            <div class="icon"><i class="fa fa-list"></i></div>
            <h4 class="title"><a href="">Misi</a></h4>
            <ol type="" style="margin-left: 55px; line-height: 24px; font-size: 14px; margin-bottom: 0px;">
              <li>Indonesia yang cerdas</li>
              <li>Organisasi yang cerdas</li>
              <li>Fasilitas yang cerdas</li>
            </ol>
          </div>

          <div class="icon-box wow fadeInUp" data-wow-delay="0.4s">
            <div class="icon"><i class="fa fa-heart"></i></div>
            <h4 class="title"><a href="">Nilai</a></h4>
            <ol type="" style="margin-left: 55px; line-height: 24px; font-size: 14px; margin-bottom: 0px;">
              <li>Kecil tetapi terhubung</li>
              <li>Bekerja Bersama dengan para ahli internasional untuk mendapatkan solusi terbaik untuk masalah klien
              </li>
              <li>Bekerja dengan orang-orang dari Universitas, Industri, LSM dan lembaga pemerintah</li>
              <li>Bermitra dengan konsultan, pemasok, penyedia layanan, pengembang perangkat lunak dan perangkat
                keras, peneliti, penasihat dan profesor, insinyur dan manajer</li>
            </ol>
          </div>

        </div>

        <div class="col-lg-6 background order-lg-2 order-1 wow fadeInUp">
          <img src="img/intro-img.svg" class="img-fluid" alt="">
        </div>
      </div>

      <div class="row about-extra">
        <div class="col-lg-6 wow fadeInUp">
          <img src="img/undraw_speech_to_text_9uir.svg" width="380px" class="img-fluid" alt="">
        </div>
        <div class="col-lg-6 wow fadeInUp pt-5 pt-lg-0">
          <h4>Perjalanan Kami</h4>
          <p>
            Rendahnya daya saing indonesia di kancah internasional, kondisi aset berbagai fasilitas dan infrastruktur layanan publik yang tidak terjaga dan kurang produktif, banyaknya perusahaan yang kurang berdaya saing internasional. Hal-hal tersebut merupakan factor-faktor yang melatarbelakangi berdirinya PT. Karya Indonesia Cerdas. <br>
            <br>Dengan berbekal pengalaman internasional, jaringan global dan penguasaan teknologi 4.0 maka pendiri PT. Karya Indonesia Cerdas berupaya untuk megembangkan sesuatu untuk mencari solusi dalam rangka menjadikan SDM, infrastruktur dan berbagai institusi di Indonesia dapat bertindak lebih cepat, efektif dan efisien untuk memenangkan kompetisi global dalam rangka menjadikan indonesia Negara yang disegani di dunia internasional.
          </p>
        </div>
      </div>

      <!-- <div class="row about-extra">
          <div class="col-lg-6 wow fadeInUp order-1 order-lg-2">
            <img src="img/about-extra-2.svg" class="img-fluid" alt="">
          </div>

          <div class="col-lg-6 wow fadeInUp pt-4 pt-lg-0 order-2 order-lg-1">
            <h4>Visi, Misi dan Nilai</h4>
            <p>
              Delectus alias ut incidunt delectus nam placeat in consequatur. Sed cupiditate quia ea quis. Voluptas nemo
              qui aut distinctio. Cumque fugit earum est quam officiis numquam. Ducimus corporis autem at blanditiis
              beatae incidunt sunt.
            </p>
            <p>
              Voluptas saepe natus quidem blanditiis. Non sunt impedit voluptas mollitia beatae. Qui esse molestias.
              Laudantium libero nisi vitae debitis. Dolorem cupiditate est perferendis iusto.
            </p>
            <p>
              Eum quia in. Magni quas ipsum a. Quis ex voluptatem inventore sint quia modi. Numquam est aut fuga
              mollitia exercitationem nam accusantium provident quia.
            </p>
          </div>
        </div> -->

    </div>
  </section><!-- #about -->

  <!--==========================
      Services Section
    ============================-->
  <section id="services" class="section-bg">
    <div class="container">

      <header class="section-header">
        <h3>Layanan</h3>
        <p>Berikut adalah beberapa layanan dari PT. KARYA INDONESIA CERDAS :</p>
      </header>

      <div class="row">

        <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-duration="1.4s">
          <div class="box">
            <div class="icon"><i class="ion-ios-bookmarks-outline" style="color: #ff689b;"></i></div>
            <h4 class="title"><a href="studi-penelitian">Studi dan Penelitian </a></h4>
            <p class="description">Studi Kelayakan, Studi Pasar, Studi Teknologi, Studi Lingkungan, Studi Kebijakan
            </p>
          </div>
        </div>
        <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-duration="1.4s">
          <div class="box">
            <div class="icon"><i class="ion-ios-analytics-outline" style="color: #e9bf06;"></i></div>
            <h4 class="title"><a href="">Pembuatan dan Pengembangan </a></h4>
            <p class="description">Produk dan Kebijakan</p>
          </div>
        </div>

        <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
          <div class="box">
            <div class="icon"><i class="ion-ios-paper-outline" style="color: #3fcdc7;"></i></div>
            <h4 class="title"><a href="pelatihan">Pelatihan</a></h4>
            <p class="description">Teknologi dan Kebijakan</p>
          </div>
        </div>
        <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
          <div class="box">
            <div class="icon"><i class="ion-ios-speedometer-outline" style="color:#41cf2e;"></i></div>
            <h4 class="title"><a href="pengujian-evaluasi">Pengujian dan Evaluasi</a></h4>
            <p class="description">Produk Teknologi, Sistem dan Kebijakan</p>
          </div>
        </div>

        <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1.4s">
          <div class="box">
            <div class="icon"><i class="ion-ios-people-outline" style="color: #d6ff22;"></i></div>
            <h4 class="title"><a href="">Manajemen Kegiatan</a></h4>
            <p class="description">Pertemuan, Simposium, Workshop, Seminar, FGD, Konferensi, Ekspo, dan Pertandingan
              Produk</p>
          </div>
        </div>
        <div class="col-md-6 col-lg-5 wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1.4s">
          <div class="box">
            <div class="icon"><i class="ion-ios-cog-outline" style="color: #4680ff;"></i></div>
            <h4 class="title"><a href="">Suplai Peralatan dan Sistem </a></h4>
            <p class="description"></p>
          </div>
        </div>

      </div>

    </div>
  </section><!-- #services -->

  <!--==========================
      Why Us Section
    ============================-->
  <section id="why-us" class="wow fadeIn">
    <div class="container">
      <header class="section-header">
        <h3>Mengapa memilih kami ?</h3>
        <!-- <p>Laudem latine persequeris id sed, ex fabulas delectus quo. No vel partiendo abhorreant vituperatoribus.</p> -->
      </header>

      <div class="row row-eq-height justify-content-center">

        <div class="col-lg-4 mb-4">
          <div class="card wow bounceInUp">
            <i class="fa fa-dollar"></i>
            <div class="card-body">
              <h5 class="card-title">BIAYA</h5>
              <p class="card-text">Kami tidak menawarkan solusi yang murah, kami menjamin biayanya jauh lebih rendah
                dibandingkan dengan dampak yang Anda dapatkan. Biaya kami rasional dan dapat dinegosiasikan. Kami
                memberikan layanan biaya tinggi dengan harga yang dapat diterima</p>
              <!-- <a href="#" class="readmore">Read more </a> -->
            </div>
          </div>
        </div>

        <div class="col-lg-4 mb-4">
          <div class="card wow bounceInUp">
            <i class="fa fa-diamond"></i>
            <div class="card-body">
              <h5 class="card-title">KUALITAS</h5>
              <p class="card-text">Kualitas merupakan yang paling utama bagi kami. Kami telah memberikan produk dan
                layanan berkualitas di lebih dari 20 negara. Kami memahami standar kualitas dan kami selalu melampaui
                standar.
              </p>
              <!-- <a href="#" class="readmore">Read more </a> -->
            </div>
          </div>
        </div>

        <div class="col-lg-4 mb-4">
          <div class="card wow bounceInUp">
            <i class="fa fa-hourglass"></i>
            <div class="card-body">
              <h5 class="card-title">WAKTU</h5>
              <p class="card-text">Waktu adalah uang, tepat waktu selalu menjadi kebiasaan kita. Pengiriman tepat
                waktu adalah indikator kinerja utama kami. Kami selalu mendorong semua pihak yang terlibat dalam
                kontrak untuk memberikan tepat waktu.
              </p>
              <!-- <a href="#" class="readmore">Read more </a> -->
            </div>
          </div>
        </div>

      </div>

      <div class="row counters">

        <div class="col-lg-4 col-6 text-center">
          <span data-toggle="counter-up">15</span>
          <p>Klien</p>
        </div>

        <div class="col-lg-4 col-6 text-center">
          <span data-toggle="counter-up">26</span>
          <p>Proyek</p>
        </div>

        <div class="col-lg-4 col-6 text-center">
          <span data-toggle="counter-up">20</span>
          <p>Pekerja Keras</p>
        </div>

      </div>

    </div>
  </section>

  <!--==========================
      Portfolio Section
    ============================-->
  <section id="portfolio" class="section-bg">
    <div class="container">

      <header class="section-header">
        <h3 class="section-title">Our Portfolio</h3>
      </header>

      <div class="row">
        <div class="col-lg-12">
          <ul id="portfolio-flters">
            <li data-filter="*" class="filter-active">All</li>
            <li data-filter=".filter-app">App</li>
            <!-- <li data-filter=".filter-card">Card</li> -->
            <li data-filter=".filter-web">Web</li>
          </ul>
        </div>
      </div>

      <div class="row portfolio-container">

        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
          <div class="portfolio-wrap">
            <img src="img/portfolio/app1.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4><a href="#">App 1</a></h4>
              <p>App</p>
              <div>
                <a href="img/portfolio/app1.jpg" data-lightbox="portfolio" data-title="App 1" class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 portfolio-item filter-web" data-wow-delay="0.2s">
          <div class="portfolio-wrap">
            <img src="img/portfolio/siteppar.jpg" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4><a href="#">SITEPPAR</a></h4>
              <p>Web</p>
              <div>
                <a href="img/portfolio/siteppar.jpg" class="link-preview" data-lightbox="portfolio" data-title="Sistem Terpadu Pintar untuk Pengelolaan Aset Rumah Sakit" title="Preview"><i class="ion ion-eye"></i></a>
                <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>
  </section><!-- #portfolio -->

  <!--==========================
      Clients Section
    ============================-->
  <section id="testimonials" class="clearfix">
    <div class="container">

      <header class="section-header">
        <h3>Testimoni</h3>
      </header>

      <div class="row justify-content-center">
        <div class="col-lg-8">

          <div class="owl-carousel testimonials-carousel wow fadeInUp">

            <div class="testimonial-item">
              <img src="img/default.jpg" class="testimonial-img" alt="">
              <h3>Nama Pelanggan</h3>
              <h4>Nama &amp; Jabatan</h4>
              <p>
                Isi testimoni dari pelanggan.
              </p>
            </div>



          </div>

        </div>
      </div>


    </div>
  </section><!-- #testimonials -->

  <!--==========================
      Team Section
    ============================-->
  <!-- <section id="team">
      <div class="container">
        <div class="section-header">
          <h3>Tim</h3>
          <p>Tim kami terdiri dari orang-orang profesional yang ahli di bidangnya</p>
        </div>

        <div class="row">

          <div class="col-lg-3 col-md-6 wow fadeInUp">
            <div class="member">
              <img src="img/default.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Rila Siregar</h4>
                  <span>Direktur Utama</span>
                  <div class="social">
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-google-plus"></i></a>
                    <a href=""><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
            <div class="member">
              <img src="img/default.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Bertawati</h4>
                  <span>Manager Administrasi, SDM & Keuangan</span>
                  <div class="social">
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-google-plus"></i></a>
                    <a href=""><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
            <div class="member">
              <img src="img/default.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Dr. Yulizar Widyatama</h4>
                  <span>Manager Teknik dan Operasional </span>
                  <div class="social">
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.3s">
            <div class="member">
              <img src="img/default.jpg" class="img-fluid" alt="">
              <div class="member-info">
                <div class="member-info-content">
                  <h4>Adrian Portibi Siregar</h4>
                  <span>Manager Pemasaran</span>
                  <div class="social">
                    <a href=""><i class="fa fa-twitter"></i></a>
                    <a href=""><i class="fa fa-facebook"></i></a>
                    <a href=""><i class="fa fa-google-plus"></i></a>
                    <a href=""><i class="fa fa-linkedin"></i></a>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section>#team -->

  <!--==========================
      Clients Section
    ============================-->
  <section id="clients" class="section-bg">

    <div class="container">

      <div class="section-header">
        <h3>Pelanggan Kami</h3>
        <p>Pelanggan kami terdiri dari beberapa instansi seperti Pemerintah, Universitas, Perusahaan dan Komunitas.
        </p>
      </div>

      <div class="row no-gutters clients-wrap clearfix wow fadeInUp">

        <div class="col-lg-3 col-md-4 col-xs-6">
          <div class="client-logo">
            <img src="img/clients/kemenkes.png" class="img-fluid" alt="">
          </div>
        </div>

        <div class="col-lg-3 col-md-4 col-xs-6">
          <div class="client-logo">
            <img src="img/clients/kemkominfo.png" class="img-fluid" width="110px" alt="">
          </div>
        </div>

        <div class="col-lg-3 col-md-4 col-xs-6">
          <div class="client-logo">
            <img src="img/clients/ui.png" class="img-fluid" width="100px" alt="">
          </div>
        </div>

        <div class="col-lg-3 col-md-4 col-xs-6">
          <div class="client-logo">
            <img src="img/clients/unair.png" class="img-fluid" width="500px" alt="">
          </div>
        </div>

        <div class="col-lg-3 col-md-4 col-xs-6">
          <div class="client-logo">
            <img src="img/clients/Undiksha.png" class="img-fluid" width="110px" alt="">
          </div>
        </div>

        <div class="col-lg-3 col-md-4 col-xs-6">
          <div class="client-logo">
            <img src="img/clients/uwg.png" class="img-fluid" width="110px" alt="">
          </div>
        </div>

        <div class="col-lg-3 col-md-4 col-xs-6">
          <div class="client-logo">
            <img src="img/clients/pln.png" class="img-fluid" width="90px" alt="">
          </div>
        </div>

        <div class="col-lg-3 col-md-4 col-xs-6">
          <div class="client-logo">
            <img src="img/clients/toyota.png" class="img-fluid" alt="">
          </div>
        </div>

      </div>

    </div>

  </section>

  <!--==========================
      Contact Section
    ============================-->
  <section id="contact">
    <div class="container-fluid">

      <div class="section-header">
        <h3>Hubungi Kami</h3>
      </div>

      <div class="row wow fadeInUp">

        <div class="col-lg-6">
          <div class="map mb-4 mb-lg-0">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d991.535229957781!2d106.84272132914671!3d-6.245153099717464!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3b85bf3e09d%3A0x160234e756fdf17b!2sWisma%20NH!5e0!3m2!1sid!2sid!4v1573120408965!5m2!1sid!2sid" width="600" frameborder="0" style="border:0; width: 100%; height: 312px;" allowfullscreen></iframe>
          </div>
        </div>

        <div class="col-lg-6">
          <div class="row">
            <div class="col-md-5 info">
              <i class="ion-ios-location-outline"></i>
              <p>Wisma NH Building Lantai 1 Pancoran Kota, Jakarta Selatan</p>
            </div>
          </div>

          <div class="form">
            <div id="sendmessage">Your message has been sent. Thank you!</div>
            <div id="errormessage"></div>
            <form action="" method="post" role="form" class="contactForm">
              <div class="form-row">
                <div class="form-group col-lg-6">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
                </div>
                <div class="form-group col-lg-6">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                  <div class="validation"></div>
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                <div class="validation"></div>
              </div>
              <div class="text-center"><button type="submit" title="Send Message">Kirim Pesan</button></div>
            </form>
          </div>
        </div>

      </div>

    </div>
  </section><!-- #contact -->

</main>

<?php include "footer.php"; ?>